#ifndef VALUEFUNCS_H
#define VALUEFUNCS_H
#include <EEPROM.h>
#include <Homie.h>

typedef bool (*handler)(const String&, const String&, const String&, void*);

class ValueFuncs{
private:
    handler hnd;

public:
    ValueFuncs(handler h=nullptr) : hnd(h) {};

    virtual bool hasChanged(const String& p, const String& v, const String& n, void* o)const{
        if(hnd != nullptr){
            return (*hnd)(p, v, n, o);
        }
        return false;
    };
    virtual bool fromString(const char* s, void*)const{
        Homie.getLogger() << "No conversion from string "<< s << " available." << endl;
        return false;
    };
    virtual bool toString(const void*, char*)const{
        Homie.getLogger() << "No conversion to string avaiable." << endl;
        return false;
    };
    virtual int toEEPROM(const void*, int adr)const{
        Homie.getLogger() << "No storage avaiable." << endl;
        return 0;
    };
    virtual int fromEEPROM(void*, int adr)const{
        Homie.getLogger() << "No storage avaiable." << endl;
        return 0;
    };
};

class IntValue : public ValueFuncs {
public:
    IntValue(handler h=nullptr) : ValueFuncs(h) {};

    virtual bool fromString(const char* s, void* v)const{
        if(strncmp("0x",s,2) == 0)*(int*)v = strtol(s, nullptr, 16);
        else *(int*)v = atoi(s);
        return true;
    };
    virtual bool toString(const void* v, char* s)const{
        itoa(*(int*)v, s, 10);
        return true;
    };
    virtual int toEEPROM(const void* p, int adr)const{
        int i = *((int*)p);
        EEPROM.write(adr++, i & 0xff);
        i >>= 8;
        EEPROM.write(adr++, i & 0xff);
        i >>= 8;
        EEPROM.write(adr++, i & 0xff);
        i >>= 8;
        EEPROM.write(adr++, i & 0xff);
        return adr;
    };
    virtual int fromEEPROM(const void* p, int adr)const{
        int i = 0;
        i = EEPROM.read(adr++);
        i = (i<<8) + EEPROM.read(adr++);
        i = (i<<8) + EEPROM.read(adr++);
        i = (i<<8) + EEPROM.read(adr++);
        *((int*)p) = i;
        return adr;
    };
};

class ByteValue : public ValueFuncs {
public:
    ByteValue(handler h=nullptr) : ValueFuncs(h) {};

    virtual bool fromString(const char* s, void* v)const{
        if(strncmp("0x",s,2) == 0)*(byte*)v = byte(strtol(s, nullptr, 16));
        else *(byte*)v = byte(atoi(s));
        return true;
    };
    virtual bool toString(const void* v, char* s)const{
        itoa(*(int*)v, s, 10);
        return true;
    };
};

#endif
