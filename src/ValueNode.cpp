#include <pgmspace.h>
#include <math.h>
#include <Embedis.h>
#include <ValueNode.hpp>



ByteValue* ValueNode::byteV;
IntValue* ValueNode::intV;

Value* ValueNode::findValue(const void* v)const{
    for (unsigned i=0; i<values.size(); i++){
        if(values.at(i)->getV() == v)return values.at(i);
    }
    return nullptr;
}
Value* ValueNode::findName(const char* n)const{
    Homie.getLogger() << "ValueNode: searching name: "<< n << endl;
    for (unsigned i=0; i<values.size(); i++){
        const __FlashStringHelper* np = values.at(i)->getN();
        if(strcmp_P(n, (PGM_P)np) == 0)return values.at(i);
    }
    Homie.getLogger() << "ValueNode: no name found. " << endl;
    return nullptr;
}

Value* ValueNode::findName(const __FlashStringHelper* n)const{
    char buf[32];
    strncpy_P(buf, (PGM_P)n, sizeof(buf));
    Homie.getLogger() << "ValueNode: searching name: "<< buf << endl;
    for (unsigned i=0; i<values.size(); i++){
        const __FlashStringHelper* np = values.at(i)->getN();
        if(strcmp_P(buf, (PGM_P)np) == 0)return values.at(i);
    }
    Homie.getLogger() << "ValueNode: no name found. " << endl;
    return nullptr;
}

Value*  ValueNode::addValue(const __FlashStringHelper* name, void* val, const ValueFuncs* f){
    char buf[32];
    strncpy_P(buf, (PGM_P)name, sizeof(buf));
    if(findName(buf) != nullptr){
        Homie.getLogger() << "ValueNode: trying to add exsiting value name: "<< buf << endl;
        return nullptr;
    }

    Value* v = new Value(name, val, f);
    values.push_back(v);
    recover(v);

    advertise(buf).settable();
    Homie.getLogger() << "ValueNode("<< values.size() << "): added:" << buf  << endl;
    return v;
};

void ValueNode::updateName(const __FlashStringHelper* name)const{
    char buf[32];
    strncpy_P(buf, (PGM_P)name, sizeof(buf));
    const Value* v = findName(buf);
    if(v == nullptr){
        Homie.getLogger() << "ValueNode: trying to update unknown value name: "<< buf << endl;
        return;
    }
    update(v);
};

void ValueNode::updateValue(const void* val)const{
    const Value* v = findValue(val);
    if(v == nullptr){
        Homie.getLogger() << "ValueNode: trying to update unknown value reference."<< endl;
        return;
    }
    update(v);
};

void ValueNode::update(const Value* v)const{
    static char buff[16];
    static char name[32];
    v->toString(buff);
    strncpy_P(name, (PGM_P)(v->getN()), sizeof(name));
    setProperty(name).send(buff);
    if(v->getSave()){
        Embedis::set(name, buff);
    }
}

bool ValueNode::recover(Value* v){
    char buff[16];
    if(Embedis::get(v->getN(), buff)){
        return v->fromString(buff);
    }
    else{
        v->toString(buff);
        return Embedis::set(v->getN(), buff);
    }
} 

bool ValueNode::handleInput(const String& property, const HomieRange& range, const String& value) {
    if(range.isRange) return false;

//    Homie.getLogger() << "ValueNode("<< values.size() << "): handleInput:" << property << " ist keine range." << endl;

    Value* v = (Value*)findName(property.c_str());
    if(v != nullptr){
//        Homie.getLogger() << "ValueNode: found property " << property << endl;
        String nn(getId());

        if(!v->call(nn, property, value)){
            static char buff[16];
//            Homie.getLogger() << "ValueNode: transform string to value " << value.c_str() << endl;
            v->fromString(value.c_str());
            v->toString(buff);
//            Homie.getLogger() << "ValueNode: got in buffer " << buff << endl;
            setProperty(v->getN()).send(buff);
//            Homie.getLogger() << "ValueNode("<< values.size() << "): setValue:" << nn << " with value:" << buff << endl;
        }
        return true;
    }
    return false;
}

// Embedis will run on the Serial port. Use the Arduino
// serial monitor and send "COMMANDS" to get started.
// Make sure "No line ending" is -not- selected. All others work.
Embedis _embedis(Serial);

void ValueNode::process(){
    _embedis.process();
}

void ValueNode::initEmbedis(){
   // Create a key-value Dictionary in emulated EEPROM (FLASH actually...)
    Homie.getLogger() << "Embedis init start ...." << endl;
    EEPROM.begin(SPI_FLASH_SEC_SIZE);
    Embedis::dictionary( 
        "EEPROM",
        SPI_FLASH_SEC_SIZE,
        [](size_t pos) -> char { return EEPROM.read(pos); },
        [](size_t pos, char value) { EEPROM.write(pos, value); },
        []() { EEPROM.commit(); }
    );

    // Add pinMode command to mirror Arduino's
    Embedis::command( F("pinMode"), [](Embedis* e) {
        if (e->argc != 3) return e->response(Embedis::ARGS_ERROR);
        int pin = String(e->argv[1]).toInt();
        String argv3(e->argv[2]);
        argv3.toUpperCase();
        int mode;
        if (argv3 == "INPUT") mode = INPUT;
        else if (argv3 == "OUTPUT") mode = OUTPUT;
        else if (argv3 == "INPUT_PULLUP") mode = INPUT_PULLUP;
        else return e->response(Embedis::ARGS_ERROR);
        pinMode(pin, mode);
        e->response(Embedis::OK);
    });

    // Add digitalWrite command to mirror Arduino's
    Embedis::command( F("digitalWrite"), [](Embedis* e) {
        if (e->argc != 3) return e->response(Embedis::ARGS_ERROR);
        int pin = String(e->argv[1]).toInt();
        String argv3(e->argv[2]);
        argv3.toUpperCase();
        int mode;
        if (argv3 == "HIGH") mode = HIGH;
        else if (argv3 == "LOW") mode = LOW;
        else mode = argv3.toInt();
        digitalWrite(pin, mode);
        e->response(Embedis::OK);
    });

    // Add digitalRead command to mirror Arduino's
    Embedis::command( F("digitalRead"), [](Embedis* e) {
        if (e->argc != 2) return e->response(Embedis::ARGS_ERROR);
        int pin = String(e->argv[1]).toInt();
        if (digitalRead(pin)) {
            e->response(F("HIGH"));
        } else {
            e->response(F("LOW"));
        }
    });

    // Add analogRead command to mirror Arduino's
    Embedis::command( F("analogRead"), [](Embedis* e) {
        if (e->argc != 2) return e->response(Embedis::ARGS_ERROR);
        int pin = String(e->argv[1]).toInt();
        e->response(':', analogRead(pin));
    });    
    Homie.getLogger() << ".....Embedis init end." << endl;
}

