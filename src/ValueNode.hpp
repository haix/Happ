#ifndef VALUENODE_H
#define VALUENODE_H

#include <arduino.h>
#include <EEPROM.h>
#include <Homie.h>
#include <vector>
#include <ValueFuncs.h>
#include <Embedis.h>
/* Test for ESP platform */
#if defined(ARDUINO_ARCH_ESP8266)
#include <EEPROM.h>
#include "spi_flash.h"
#else
#error "Please use the specific example for your board type - this example is for ESP8266 only..."
#endif



class Value {
    public:
//        Value(const char* n, void* p, const ValueFuncs* f) :
//            name(n), value(p), fnc(f) {};

        Value( const __FlashStringHelper* n, void* p, const ValueFuncs* f) :
            name(n), value(p), fnc(f) {};

        bool call(const String& no, const String& pr, const String& n){
            return fnc->hasChanged(no, pr, n, value);
        };
        const void* getV()const{return value;};
        const __FlashStringHelper* getN()const{return name;};
        bool fromString(const char* s){
            return fnc->fromString(s, value);
        };
        bool toString(char* s)const{
            return fnc->toString(value, s);
        };
        bool getSave()const{
            return save;
        };
        void setSave(bool b){
            save = b;
        }
        // const int toEEPROM(int adr)const{
        //     if(save)
        //         return fnc->toEEPROM(value, adr);
        //     else
        //         return adr;
        // };
        // int fromEEPROM(int adr){
        //     if(save)
        //         return fnc->fromEEPROM(value, adr);
        //     else
        //         return adr;
        // };

    private:
        const __FlashStringHelper* name;
        void* value;
        const ValueFuncs* fnc;
        bool save;
};

const int EEPROMSIZE = 1024;

class ValueNode : public HomieNode {
    public:
        ValueNode(const char* n="values"):HomieNode(n, "default"){
            byteV = new ByteValue();
            intV = new IntValue();
            initEmbedis();
        };

        void process();

        Value* addByteValue(const __FlashStringHelper* name, byte* val){
            return addValue(name, val, byteV);
        };

        Value* addByteValue(const __FlashStringHelper* name, byte* val, handler hnd){
            ByteValue* bV = new ByteValue(hnd);
            return addValue(name, val, bV);
        };

        Value* addIntValue(const __FlashStringHelper* name, int* val){
            return addValue(name, val, intV);
        };

        Value* addIntValue(const __FlashStringHelper* name, int* val, handler hnd){
            IntValue* iV = new IntValue(hnd);
            return addValue(name, val, iV);
        };

        Value* addValue(const __FlashStringHelper* name, void* val, const ValueFuncs* f);

        void updateName(const __FlashStringHelper*)const;

        void updateValue(const void*)const;

    protected:
        virtual bool handleInput(const String& property, const HomieRange& range, const String& value);

    private:
        Value* findValue(const void*)const;
        Value* findName(const char*)const;
        Value* findName(const __FlashStringHelper*)const;
        void update(const Value*)const;
        void initEmbedis();

        bool recover(Value*);
//        int toEEPROM(int v, int adr=0)const;
//        int fromEEPROM(int& v, int adr = 0);

        static ByteValue* byteV;
        static IntValue* intV;
        std::vector<Value*> values;

};

#endif
